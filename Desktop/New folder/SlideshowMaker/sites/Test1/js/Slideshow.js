/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


   
   var xmlhttp = new XMLHttpRequest();
    var url = "./js/jsonslides.json";
    var jsonArray;
    
    
    //var text = '{"title":"test", "slides":[' + '{"caption":"s1","src":"img/ArchesUtah.jpg" },' + '{"caption":"s2","src":"img/BadlandsSouthDakota.jpg" },' + '{"caption":"s3","src":"img/BryceCanyonUtah.jpg" }]}';
    
    xmlhttp.onreadystatechange = function(){
        
        jsonArray = JSON.parse(xmlhttp.responseText);
        display(jsonArray);
        
    
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    
    var play = 0;
    var show;
    var index = 0;

    
    
/*obj = JSON.parse(text);
    document.getElementById("caption").innerHTML = obj.slides[0].caption;
    document.getElementById("title").innerHTML = obj.title;
    document.getElementById("image").src = obj.slides[0].src;
    */
    
    
function display(arr){
   
    document.getElementById("caption").innerHTML = arr.slides[0].caption;
    document.getElementById("image").src = "img/" + arr.slides[0].image_file_name;
    document.getElementById("title").innerHTML = arr.title;
    
}

function playPause(){
    var icon = document.getElementById("playpausepic");
    if(play === 0){
        icon.src ="./icons/pause.png";
        play++;
        show = setInterval(nextSlide, 1000);
    }
    else{
        icon.src = "./icons/play.png";
        play--;
        clearInterval(show);
    }
}

function nextSlide(){
    var size = jsonArray.slides.length - 1;
    if(index === size){
        display(jsonArray);
        index = 0;
    }
    else{
        document.getElementById("caption").innerHTML = jsonArray.slides[index + 1].caption;
        document.getElementById("image").src = "img/" + jsonArray.slides[index + 1].image_file_name;
        index++;
    }
        
}

function previousSlide(){
    var size = jsonArray.slides.length - 1;
    if(index === 0){
        document.getElementById("caption").innerHTML = jsonArray.slides[size].caption;
        document.getElementById("image").src = "img/" + jsonArray.slides[size].image_file_name;
        index = size;
    }
    else{
        document.getElementById("caption").innerHTML = jsonArray.slides[index - 1].caption;
        document.getElementById("image").src = "img/" + jsonArray.slides[index - 1].image_file_name;
        index--;
    }
    
}